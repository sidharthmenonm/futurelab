import csvtojson from './csvtojson';
import Vue from 'vue';

new Vue({ 
  el: '#app',
  data: {
    data: [],
    message: [],
    key: '2PACX-1vRXEUy4-ZvXaE75TRNdfexFZzjIYiBcV5qHcMbFgVGVJ3Pr_5d-ajC92BKFKD43AYYmyMdYXUN6MUBA',
  },
  computed: {
    header(){
      return this.sort('header')[0];
    },
    initiatives_header(){
      return this.sort('initiatives_header')[0];
    },
    initiatives(){
      return this.sort('initiatives_items');
    },
    facilities(){
      return this.sort('facilities');
    }
  },
  methods: {
    sort: function(typ) {
      var sorted = this.data.filter(function(item){
        return item.type == typ;
      })

      // // Set slice() to avoid to generate an infinite loop!
      // sorted = sorted.slice().sort(function(a, b) {
      //   return a.order - b.order;
      // });

      // return sorted.filter(function(item){
      //   return item.order>0
      // })
      return sorted;
    },
    loadData: function(data,gid){
      var vm = this;
      fetch(`https://docs.google.com/spreadsheets/d/e/${this.key}/pub?gid=${gid}&single=true&output=csv`)
      .then(response => response.text())
      .then(csv => csvtojson(csv))
      .then(function(json){
        vm[data] = JSON.parse(json);
      });
    }

  },
  mounted(){
    this.loadData('message', '0');
    this.loadData('data', '1385298544');
  }
})